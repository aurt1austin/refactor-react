import React from 'react';
import styled from 'styled-components';
import { RobotoNormalBlueberry16px, RobotoNormalBlack14px, KanitR14 } from '../../../../styledMixins';


function AmountEachYearTableDetail(props) {
  const { className } = props;

  return (
    <DivWrapper className={`group-226 ${className || ""}`}>
      <Column1 className="number-11">1</Column1>
      <Column2 className="pr20210119001">DocumentNo</Column2>
      <Column3 className="text-5">ชุดการเรียนการสอน สำหรับ...</Column3>
      <Column4 className="cmm-1">CMM</Column4>
      <Column5 className="username01">Username01</Column5>
      <Column6 className="price">฿10,000.00</Column6>
    </DivWrapper>
  );
}

const DivWrapper = styled.div`
  height: 21px;
  display: flex;
  align-items: center;
  min-width: 685px;

  &.group-226.group-227 {
    margin-top: 4px;
  }

  &.group-226.group-228 {
    margin-top: 4px;
  }

  &.group-226.group-229 {
    margin-top: 4px;
  }

  &.group-226.group-230 {
    margin-top: 7px;
  }

  &.group-226.group-231 {
    margin-top: 4px;
  }
`;

const Column1 = styled.div`
  ${RobotoNormalBlack14px}
  min-height: 16px;
  margin-top: 1px;
  min-width: 8px;
  letter-spacing: 0;
`;

const Column2 = styled.div`
  ${RobotoNormalBlueberry16px}
  min-height: 19px;
  margin-left: 26px;
  min-width: 119px;
  letter-spacing: 0;
`;

const Column3 = styled.div`
  ${RobotoNormalBlack14px}
  min-height: 16px;
  margin-left: 26px;
  margin-bottom: 1px;
  min-width: 163px;
  letter-spacing: 0;
`;

const Column4 = styled.div`
  ${RobotoNormalBlack14px}
  min-height: 16px;
  margin-left: 26px;
  margin-bottom: 1px;
  min-width: 34px;
  letter-spacing: 0;
`;

const Column5 = styled.div`
  ${RobotoNormalBlack14px}
  min-height: 16px;
  margin-left: 59px;
  margin-bottom: 1px;
  min-width: 80px;
  letter-spacing: 0;
`;

const Column6 = styled.div`
  ${KanitR14}
  min-height: 21px;
  margin-left: 59px;
  min-width: 73px;
  font-weight: 400;
  color: var(--black);
`;

export default AmountEachYearTableDetail;
