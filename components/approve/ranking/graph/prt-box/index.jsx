import React from "react";
import styled from "styled-components";
import { KanitR12 } from "../../../../../styledMixins";


function PRTBox() {
  return (
    <PRTBoxWrapper>
      <Box></Box>
      <PRT>PRT</PRT>
    </PRTBoxWrapper>
  );
}

const PRTBoxWrapper = styled.div`
  position: absolute;
  height: 14px;
  top: 15px;
  left: 645px;
  display: flex;
  align-items: center;
  min-width: 39px;
`;

const Box = styled.div`
  width: 10px;
  height: 10px;
  margin-bottom: 0.19px;
  background-color: var(--magenta--fuchsia);
`;

const PRT = styled.div`
  ${KanitR12}
  min-height: 14px;
  margin-left: 5px;
  min-width: 22px;
  font-weight: 400;
  color: var(--grayscalegray1);
  line-height: 14px;
  white-space: nowrap;
`;

export default PRTBox;
