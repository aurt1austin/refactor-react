import React from 'react';
import styled from 'styled-components';
import { KanitR12 } from '../../../../../styledMixins';


function CMMBox() {
  return (
    <CMMBoxWrapper>
      <Box></Box>
      <CMM>CMM</CMM>
    </CMMBoxWrapper>
  );
}

const CMMBoxWrapper = styled.div`
  position: absolute;
  height: 14px;
  top: 0;
  left: 645px;
  display: flex;
  align-items: center;
  min-width: 43px;
`;

const Box = styled.div`
  width: 10px;
  height: 10px;
  background-color: var(--aquamarine);
`;

const CMM = styled.div`
  ${KanitR12}
  min-height: 14px;
  margin-left: 5px;
  min-width: 26px;
  font-weight: 400;
  color: var(--grayscalegray1);
  line-height: 14px;
  white-space: nowrap;
`;

export default CMMBox;
