import React from 'react';
import styled from 'styled-components';
import { KanitR12 } from '../../../../../styledMixins';


function ECTBox() {
  return (
    <ECTBoxWrapper>
      <Box></Box>
      <ECT>ECT</ECT>
    </ECTBoxWrapper>
  );
}

const ECTBoxWrapper = styled.div`
  position: absolute;
  height: 14px;
  top: 32px;
  left: 645px;
  display: flex;
  align-items: center;
  min-width: 38px;
`;

const Box = styled.div`
  width: 10px;
  height: 10px;
  margin-bottom: 0.38px;
  background-color: var(--electric-violet);
`;

const ECT = styled.div`
  ${KanitR12}
  min-height: 14px;
  margin-left: 5px;
  min-width: 21px;
  font-weight: 400;
  color: var(--grayscalegray1);
  line-height: 14px;
  white-space: nowrap;
`;

export default ECTBox;
