import React from 'react';
import HeaderNav from '../../header-nav';
import MainNav from '../../main-nav';
import PRTBox from './graph/prt-box';
import CMMBox from './graph/cmm-box';
import ECTBox from './graph/ect-box';
import Breadcrumbs from '../../breadcumbs';
import AmountEachYearTableDetail from './amout-each-year-table-detail';
import RankingHistory from './ranking-history';
import Reason1 from './reason/reason1';
import Reason2 from './reason/reason2';
import ProgressDetail from './reason/progress-detail';
import Reason4 from './reason/reason4';
import RankingResultStatus from './ranking-result/ranking-result-status';
import RankingResultTableDetail from './ranking-result/ranking-result-table';
import InputApproveRemark from './input-approve';
import RejectButton from './input-approve/reject-btn';
import ApproveButton from './input-approve/approve-btn';
import styled from 'styled-components';
import {
  KanitR12,
  SarabunR12,
  KanitR14,
  Border1pxCararra,
  ArialNarrowBoldAbbey12px,
  KanitR24,
  PoppinsMediumDeepCove128px,
  ArialNarrowBoldMountainMist12px,
  PoppinsSemiBoldDeepCove173px,
  AbelNormalAbbey20px,
  PoppinsMediumFrenchGray128px,
  ValignTextMiddle,
} from '../../../styledMixins';
import './styles.css';

function Ranking(props) {
  const {
    graphHeaderTitle,
    graphHeaderSubTitle,
    sortBy1,
    monthly1,
    scaleY1,
    scaleY2,
    scaleY3,
    scaleY4,
    scaleY5,
    scaleY6,
    scaleX1,
    scaleX2,
    scaleX3,
    scaleX4,
    scaleX5,
    amountEachYearTitle,
    sortBy2,
    monthly2,
    amountEachYearColumn1,
    amountEachYearColumn2,
    amountEachYearColumn3,
    amountEachYearColumn4,
    amountEachYearColumn5,
    amountEachYearColumn6,
    rankingHistoryTitle,
    reasonTitle,
    place,
    percent,
    rankinResultTitle,
    rankingResultColumn1,
    rankingResultColumn2,
    rankingResultColumn3,
    rankingResultColumn4,
    rankingResultColumn5,
    rankingResultColumn6,
    rankingResultColumn7,
    approveTitle,
    approveRemark,
    mainNavProps,
    breadcrumbsProps,
    amoutTabelDetailProps1,
    amoutTabelDetailProps2,
    amoutTabelDetailProps3,
    amoutTabelDetailProps4,
    amoutTabelDetailProps5,
    reasonProps1,
    reasonProps2,
    reasonProps3,
    reasonProps4,
    rankingResultProps1,
    rankingResultProps2,
    rankingResultProps3,
    rankingResultProps4,
    rankingResultProps5,
    rankingResultProps6,
    rankingResultProps7,
    InputApproveRemarkProps,
    rejectButtonProps,
    approveButtonProps,
  } = props;

  return (
    <div className="container-center-horizontal">
      <div className="ranking screen">
        <HeaderNav />
        <MainNav {...mainNavProps} />
        <RankingSummary>
          <RankingSummaryLeft>
            <SummaryUpperZone>
              <SummaryGraph>
                <GraphHeader>
                  <GraphHeaderLeft>
                    <GraphHeaderTitle>{graphHeaderTitle}</GraphHeaderTitle>
                    <GraphHeaderSubTitle>
                      {graphHeaderSubTitle}
                    </GraphHeaderSubTitle>
                  </GraphHeaderLeft>
                  <GraphHeaderRight>
                    <SortBy>{sortBy1}</SortBy>
                    <Monthly>{monthly1}</Monthly>
                    <Vector src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/vector@2x.svg" />
                  </GraphHeaderRight>
                </GraphHeader>
                <GraphContainer>
                  <GraphScaleYContainer>
                    <GraphScaleY>{scaleY1}</GraphScaleY>
                    <GraphScaleY>{scaleY2}</GraphScaleY>
                    <GraphScaleY>{scaleY3}</GraphScaleY>
                    <GraphScaleY>{scaleY4}</GraphScaleY>
                    <GraphScaleY>{scaleY5}</GraphScaleY>
                    <GraphScaleY>{scaleY6}</GraphScaleY>
                  </GraphScaleYContainer>
                  <GraphPlot>
                    <GraphPlotContainer>
                      <PRTBox />
                      <GraphImage src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-262@1x.svg" />
                    </GraphPlotContainer>
                    <CMMBox />
                    <ECTBox />
                  </GraphPlot>
                </GraphContainer>
                <GraphScaleXContainer>
                  <GraphScaleX>{scaleX1}</GraphScaleX>
                  <GraphScaleX>{scaleX2}</GraphScaleX>
                  <GraphScaleX>{scaleX3}</GraphScaleX>
                  <GraphScaleX>{scaleX4}</GraphScaleX>
                  <GraphScaleX>{scaleX5}</GraphScaleX>
                </GraphScaleXContainer>
              </SummaryGraph>
              <DocumentSummary>
                <Document1>
                  <Document1Wrapper>
                    <Document1Icon src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/casg@2x.svg" />
                  </Document1Wrapper>
                </Document1>
                <Document2>
                  <Document2Wrapper>
                    <Document2Icon src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-69@2x.svg" />
                  </Document2Wrapper>
                </Document2>
                <Document3>
                  <Document3Wrapper>
                    <Document3Icon src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/tick@2x.svg" />
                  </Document3Wrapper>
                </Document3>
                <Document4>
                  <Document4Wrapper>
                    <Document4Icon src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/edit@2x.svg" />
                  </Document4Wrapper>
                </Document4>
              </DocumentSummary>
              <Breadcrumbs {...breadcrumbsProps} />
            </SummaryUpperZone>
            <AmountEachYear>
              <AmountEachYearHeader>
                <AmountEachYearTitle>{amountEachYearTitle}</AmountEachYearTitle>
                <AmountEachYearHeaderSorting>
                  <SortBy>{sortBy2}</SortBy>
                  <Monthly1>{monthly2}</Monthly1>
                  <Vector src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/vector@2x.svg" />
                </AmountEachYearHeaderSorting>
              </AmountEachYearHeader>
              <AmountEachYearColumns>
                <AmountEachYearColumn1>
                  {amountEachYearColumn1}
                </AmountEachYearColumn1>
                <AmountEachYearColumn2>
                  {amountEachYearColumn2}
                </AmountEachYearColumn2>
                <AmountEachYearColumn3>
                  {amountEachYearColumn3}
                </AmountEachYearColumn3>
                <AmountEachYearColumn4>
                  {amountEachYearColumn4}
                </AmountEachYearColumn4>
                <AmountEachYearColumn5>
                  {amountEachYearColumn5}
                </AmountEachYearColumn5>
                <AmountEachYearColumn6>
                  {amountEachYearColumn6}
                </AmountEachYearColumn6>
              </AmountEachYearColumns>
              <AmountEachYearTable>
                <AmountEachYearTableDetail />
                <AmountEachYearTableDetail
                  className={amoutTabelDetailProps1.className}
                />
                <AmountEachYearTableDetail
                  className={amoutTabelDetailProps2.className}
                />
                <AmountEachYearTableDetail
                  className={amoutTabelDetailProps3.className}
                />
                <AmountEachYearTableDetail
                  className={amoutTabelDetailProps4.className}
                />
                <AmountEachYearTableDetail
                  className={amoutTabelDetailProps5.className}
                />
              </AmountEachYearTable>
            </AmountEachYear>
          </RankingSummaryLeft>
          <RankingSummaryRight>
            <RankingHistoryWrapper>
              <RankingHistoryTitle>{rankingHistoryTitle}</RankingHistoryTitle>
              <RankingHistory />
            </RankingHistoryWrapper>
            <Reason>
              <ReasonTitle>{reasonTitle}</ReasonTitle>
              <ReasonWrapper>
                <Reason1 place={reasonProps1.place} />
                <Reason2 place={reasonProps2.place} />
                <Reason3>
                  <ReasonText>
                    <Place>{place}</Place>
                    <Percent>{percent}</Percent>
                  </ReasonText>
                  <ProgressDetail className={reasonProps3.className} />
                </Reason3>
              </ReasonWrapper>
              <Reason4 place={reasonProps4.place} />
            </Reason>
          </RankingSummaryRight>
        </RankingSummary>

        <DivMargin />

        <RankinResultHeader>
          <RankinResultTitle>{rankinResultTitle}</RankinResultTitle>
          <RankingResultStatus />
        </RankinResultHeader>
        <RankingResultTable>
          <RankingResultColumns>
            <RankingResultColumn1>{rankingResultColumn1}</RankingResultColumn1>
            <RankingResultColumn2>{rankingResultColumn2}</RankingResultColumn2>
            <RankingResultColumn3>{rankingResultColumn3}</RankingResultColumn3>
            <RankingResultColumn4>{rankingResultColumn4}</RankingResultColumn4>
            <RankingResultColumn5>{rankingResultColumn5}</RankingResultColumn5>
            <RankingResultColumn6>{rankingResultColumn6}</RankingResultColumn6>
            <RankingResultColumn7>{rankingResultColumn7}</RankingResultColumn7>
          </RankingResultColumns>
          <RankingResultTableBody>
            <RankingResultTableDetail {...rankingResultProps1} />
            <RankingResultTableDetail {...rankingResultProps2} />
            <RankingResultTableDetail {...rankingResultProps3} />
            <RankingResultTableDetail {...rankingResultProps4} />
            <RankingResultTableDetail {...rankingResultProps5} />
            <RankingResultTableDetail {...rankingResultProps6} />
            <RankingResultTableDetail {...rankingResultProps7} />
          </RankingResultTableBody>
        </RankingResultTable>
        <Approve>
          <ApproveWrapper>
            <ApproveTitle>{approveTitle}</ApproveTitle>
            <HorizontalRuler src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/line-12-20@1x.svg" />
            <ApproveHeader>
              <ApproveRemark>{approveRemark}</ApproveRemark>
              <InputApproveRemark
                text_Label={InputApproveRemarkProps.text_Label}
                placeholder={InputApproveRemarkProps.placeholder}
              />
            </ApproveHeader>
            <ApproveButtonZone>
              <RejectButton>
                {rejectButtonProps.children}
              </RejectButton>
              <ApproveButton>
                {approveButtonProps.children}
              </ApproveButton>
            </ApproveButtonZone>
          </ApproveWrapper>
        </Approve>
      </div>
    </div>
  );
}

const RankingSummary = styled.div`
  height: 735px;
  z-index: 1;
  margin-top: 77px;
  margin-right: 26px;
  display: flex;
  align-items: flex-end;
  min-width: 1075px;
`;

const RankingSummaryLeft = styled.div`
  width: 801px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 735px;
`;

const SummaryUpperZone = styled.div`
  width: 900px;
  height: 463px;
  position: relative;
  margin-top: -2px;
  margin-left: -51px;
`;

const SummaryGraph = styled.div`
  position: absolute;
  width: 799px;
  top: 161px;
  left: 53px;
  display: flex;
  flex-direction: column;
  padding: 15.2px 16.5px;
  align-items: flex-start;
  min-height: 302px;
  background-color: var(--palettewhite);
  box-shadow: 0px 15.017667770385742px 52.56183624267578px #5559911a;
`;

const GraphHeader = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: 762px;
`;

const GraphHeaderLeft = styled.div`
  width: 102px;
  height: 47px;
  position: relative;
`;

const GraphHeaderTitle = styled.div`
  ${PoppinsSemiBoldDeepCove173px}
  position: absolute;
  width: 102px;
  top: 0;
  left: 0;
  letter-spacing: 0;
`;

const GraphHeaderSubTitle = styled.div`
  position: absolute;
  width: 86px;
  top: 26px;
  left: 0;
  font-family: var(--font-family-hind);
  font-weight: 400;
  color: var(--french-gray);
  font-size: var(--font-size-s);
  letter-spacing: 0.26px;
`;

const GraphHeaderRight = styled.div`
  height: 20px;
  margin-left: 543px;
  margin-top: 2.25px;
  display: flex;
  align-items: flex-start;
  min-width: 117px;
`;

const SortBy = styled.div`
  ${PoppinsMediumFrenchGray128px}
  width: 48px;
  min-height: 20px;
  letter-spacing: 0;
`;

const Monthly = styled.div`
  ${PoppinsMediumDeepCove128px}
  width: 52px;
  min-height: 20px;
  align-self: flex-end;
  margin-left: 4px;
  margin-bottom: 0;
  letter-spacing: 0;
`;

const Vector = styled.img`
  width: 7px;
  height: 4px;
  align-self: center;
  margin-left: 3px;
  margin-top: 3.36px;
`;

const GraphContainer = styled.div`
  height: 188px;
  margin-top: 3px;
  margin-left: 0.48px;
  display: flex;
  align-items: flex-start;
  min-width: 740px;
`;

const GraphScaleYContainer = styled.div`
  ${KanitR12}
  width: 37px;
  align-self: flex-end;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  min-height: 168px;
`;

const GraphScaleY = styled.div`
  width: 37px;
  min-height: 14px;
  font-weight: 400;
  color: var(--grayscalegray1);
  text-align: right;
  line-height: 14px;
  white-space: nowrap;
`;

const GraphPlot = styled.div`
  width: 701px;
  height: 184px;
  position: relative;
  margin-left: 3px;
`;

const GraphPlotContainer = styled.div`
  position: absolute;
  width: 701px;
  height: 183px;
  top: 1px;
  left: 0;
`;

const GraphImage = styled.img`
  position: absolute;
  width: 584px;
  height: 183px;
  top: 0;
  left: 34px;
`;

const GraphScaleXContainer = styled.div`
  ${KanitR12}
  height: 14px;
  align-self: center;
  margin-top: 8px;
  margin-right: 44px;
  display: flex;
  align-items: flex-start;
  min-width: 601px;
`;

const GraphScaleX = styled.div`
  width: 33px;
  min-height: 14px;
  font-weight: 400;
  color: var(--grayscalegray1);
  text-align: center;
  line-height: 14px;
  white-space: nowrap;
`;

const DocumentSummary = styled.div`
  position: absolute;
  width: 900px;
  height: 206px;
  top: 0;
  left: 0;
`;

const Document1 = styled.div`
  position: absolute;
  height: 203px;
  top: 0;
  left: 0;
  display: flex;
  padding: 66.9px 69.1px;
  align-items: flex-start;
  min-width: 285px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-62-3@2x.svg);
  background-size: 100% 100%;
`;

const Document1Wrapper = styled.div`
  height: 40px;
  display: flex;
  padding: 11.1px 10px;
  align-items: flex-end;
  min-width: 44px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/ellipse-9-3@2x.svg);
  background-size: 100% 100%;
`;

const Document1Icon = styled.img`
  width: 23px;
  height: 17px;
`;

const Document2 = styled.div`
  position: absolute;
  height: 203px;
  top: 2px;
  left: 205px;
  display: flex;
  padding: 66.9px 69.1px;
  align-items: flex-start;
  min-width: 285px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-61-3@2x.svg);
  background-size: 100% 100%;
`;

const Document2Wrapper = styled.div`
  height: 40px;
  display: flex;
  padding: 9.6px 12.7px;
  align-items: flex-start;
  min-width: 44px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/ellipse-10-3@2x.svg);
  background-size: 100% 100%;
`;

const Document2Icon = styled.img`
  width: 18px;
  height: 19px;
`;

const Document3 = styled.div`
  position: absolute;
  height: 203px;
  top: 3px;
  left: 410px;
  display: flex;
  padding: 66.9px 69.1px;
  align-items: flex-start;
  min-width: 285px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-60-3@2x.svg);
  background-size: 100% 100%;
`;

const Document3Wrapper = styled.div`
  height: 40px;
  display: flex;
  padding: 10.9px 12.5px;
  align-items: flex-start;
  min-width: 44px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/ellipse-11-3@2x.svg);
  background-size: 100% 100%;
`;

const Document3Icon = styled.img`
  width: 18px;
  height: 17px;
`;

const Document4 = styled.div`
  position: absolute;
  height: 203px;
  top: 1px;
  left: 615px;
  display: flex;
  padding: 66.9px 69.1px;
  align-items: flex-start;
  min-width: 285px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/group-57-3@2x.svg);
  background-size: 100% 100%;
`;

const Document4Wrapper = styled.div`
  height: 40px;
  display: flex;
  padding: 10.8px 12.4px;
  align-items: flex-start;
  min-width: 44px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/ellipse-12-3@2x.svg);
  background-size: 100% 100%;
`;

const Document4Icon = styled.img`
  width: 19px;
  height: 17px;
`;

const AmountEachYear = styled.div`
  width: 799px;
  margin-top: 25px;
  display: flex;
  flex-direction: column;
  padding: 7px 18.1px;
  align-items: center;
  min-height: 249px;
  background-color: var(--palettewhite);
  box-shadow: 0px 15.017667770385742px 52.56183624267578px #5559911a;
`;

const AmountEachYearHeader = styled.div`
  height: 26px;
  align-self: flex-end;
  display: flex;
  align-items: center;
  min-width: 762px;
`;

const AmountEachYearTitle = styled.div`
  ${PoppinsSemiBoldDeepCove173px}
  width: 287px;
  min-height: 26px;
  letter-spacing: 0;
`;

const AmountEachYearHeaderSorting = styled.div`
  height: 20px;
  margin-left: 358px;
  margin-bottom: 1.65px;
  display: flex;
  align-items: flex-start;
  min-width: 117px;
`;

const Monthly1 = styled.div`
  ${PoppinsMediumDeepCove128px}
  width: 52px;
  min-height: 20px;
  margin-left: 4px;
  margin-top: 0;
  letter-spacing: 0;
`;

const AmountEachYearColumns = styled.div`
  ${KanitR12}
  height: 14px;
  margin-top: 13px;
  margin-right: 43px;
  display: flex;
  align-items: flex-start;
  min-width: 668px;
`;

const AmountEachYearColumn1 = styled.div`
  min-height: 14px;
  min-width: 28px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearColumn2 = styled.div`
  min-height: 14px;
  margin-left: 12px;
  min-width: 62px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearColumn3 = styled.div`
  min-height: 14px;
  margin-left: 83px;
  min-width: 52px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearColumn4 = styled.div`
  min-height: 14px;
  margin-left: 132px;
  min-width: 39px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearColumn5 = styled.div`
  min-height: 14px;
  margin-left: 77px;
  min-width: 23px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearColumn6 = styled.div`
  min-height: 14px;
  margin-left: 98px;
  font-weight: 400;
  color: var(--black);
  line-height: 14px;
  white-space: nowrap;
`;

const AmountEachYearTable = styled.div`
  width: 673px;
  position: relative;
  margin-top: 7px;
  margin-right: 26px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 149px;
`;

const RankingSummaryRight = styled.div`
  width: 256px;
  margin-left: 18px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 580px;
`;

const RankingHistoryWrapper = styled.div`
  ${Border1pxCararra}
  width: 256px;
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 25px 16px;
  align-items: flex-start;
  min-height: 309px;
  background-color: var(--coconut);
  border-radius: 13px;
`;

const RankingHistoryTitle = styled.div`
  ${ValignTextMiddle}
  ${AbelNormalAbbey20px}
            width: 150px;
  height: 30px;
  margin-left: 4px;
  letter-spacing: 0;
`;

const Reason = styled.div`
  ${Border1pxCararra}
  width: 256px;
  position: relative;
  margin-top: 22px;
  display: flex;
  flex-direction: column;
  padding: 5px 22px;
  align-items: flex-start;
  min-height: 249px;
  background-color: var(--coconut);
  border-radius: 13px;
`;

const ReasonTitle = styled.div`
  ${ValignTextMiddle}
  ${AbelNormalAbbey20px}
            width: 92px;
  height: 30px;
  letter-spacing: 0;
`;

const ReasonWrapper = styled.div`
  width: 140px;
  position: relative;
  margin-top: 9px;
  margin-left: 1px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 140px;
`;

const Reason3 = styled.div`
  width: 144px;
  position: relative;
  margin-top: 21px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 32px;
`;

const ReasonText = styled.div`
  width: 115px;
  height: 14px;
  position: relative;
`;

const Place = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldAbbey12px}
            position: absolute;
  width: 76px;
  height: 14px;
  top: 0;
  left: 0;
  letter-spacing: 0;
`;

const Percent = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldMountainMist12px}
            position: absolute;
  width: 40px;
  height: 14px;
  top: 0;
  left: 75px;
  text-align: right;
  letter-spacing: 0;
`;

const DivMargin = styled.img`
  width: 1061px;
  height: 1px;
  z-index: 2;
  margin-top: 42px;
  margin-right: 32px;
`;

const RankinResultHeader = styled.div`
  height: 36px;
  z-index: 3;
  position: relative;
  margin-top: 30px;
  margin-right: 32px;
  display: flex;
  align-items: center;
  min-width: 1060px;
`;

const RankinResultTitle = styled.div`
  ${KanitR24}
  min-height: 36px;
  min-width: 241px;
  font-weight: 400;
  color: var(--black);
`;

const RankingResultTable = styled.div`
  width: 1074px;
  z-index: 5;
  margin-top: 20px;
  margin-right: 27px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 525px;
`;

const RankingResultColumns = styled.div`
  ${KanitR14}
  height: 21px;
  align-self: center;
  margin-right: 153px;
  display: flex;
  align-items: flex-start;
  min-width: 717px;
`;

const RankingResultColumn1 = styled.div`
  min-height: 21px;
  min-width: 72px;
  font-weight: 400;
  color: var(--grayscalegray1);
  text-align: center;
`;

const RankingResultColumn2 = styled.div`
  min-height: 21px;
  margin-left: 59px;
  min-width: 60px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const RankingResultColumn3 = styled.div`
  min-height: 21px;
  margin-left: 133px;
  min-width: 46px;
  font-weight: 400;
  color: var(--grayscalegray1);
  text-align: center;
`;

const RankingResultColumn4 = styled.div`
  min-height: 21px;
  margin-left: 16px;
  min-width: 33px;
  font-weight: 400;
  color: var(--grayscalegray1);
  text-align: center;
`;

const RankingResultColumn5 = styled.div`
  min-height: 21px;
  margin-left: 25px;
  min-width: 26px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const RankingResultColumn6 = styled.div`
  min-height: 21px;
  margin-left: 88px;
  min-width: 73px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const RankingResultColumn7 = styled.div`
  min-height: 21px;
  margin-left: 46px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const RankingResultTableBody = styled.div`
  width: 1060px;
  position: relative;
  margin-top: 8px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 496px;
`;

const Approve = styled.div`
  height: 371px;
  z-index: 4;
  margin-top: 30px;
  margin-right: 40px;
  display: flex;
  align-items: flex-start;
  min-width: 1061px;
`;

const ApproveWrapper = styled.div`
  width: 1061px;
  display: flex;
  flex-direction: column;
  padding: 25px 32.1px;
  align-items: center;
  min-height: 371px;
  background-color: var(--palettewhite);
  border-radius: 4px;
`;

const ApproveTitle = styled.div`
  ${KanitR24}
  width: 134px;
  min-height: 36px;
  align-self: flex-start;
  margin-top: 3px;
  margin-left: 2.5px;
  font-weight: 400;
  color: var(--palettetext);
`;

const HorizontalRuler = styled.img`
  width: 992px;
  height: 1px;
  align-self: flex-end;
  margin-top: 18px;
`;

const ApproveHeader = styled.div`
  width: 699px;
  height: 180px;
  position: relative;
  margin-top: 22px;
  margin-right: 48.78px;
`;

const ApproveRemark = styled.div`
  ${SarabunR12}
  position: absolute;
  width: 49px;
  top: 3px;
  left: 91px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const ApproveButtonZone = styled.div`
  height: 36px;
  position: relative;
  margin-top: 26px;
  margin-right: 126.02px;
  display: flex;
  padding: 0 5.7px;
  justify-content: flex-end;
  align-items: flex-start;
  min-width: 354px;
`;

export default Ranking;
