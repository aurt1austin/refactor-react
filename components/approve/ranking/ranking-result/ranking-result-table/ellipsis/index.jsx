import React from 'react';
import IconImage from '../../../../../image-icon';
import styled from 'styled-components';
import { Border1pxGrayscalegray3 } from '../../../../../../styledMixins';


function Ellipsis(props) {
  const { ellipsisProps } = props;

  return (
    <More>
      <IconImage className={ellipsisProps.className} />
    </More>
  );
}

const More = styled.div`
  ${Border1pxGrayscalegray3}
  height: 24px;
  position: relative;
  align-self: center;
  margin-left: 37px;
  display: flex;
  padding: 0 2.7px;
  align-items: flex-start;
  min-width: 33px;
  background-color: var(--palettewhite);
  border-radius: 4px;
`;

export default Ellipsis;
