import React from 'react';
import Ellipsis from './ellipsis';
import styled from 'styled-components';
import { KanitR12, KanitR14 } from '../../../../../styledMixins';


function RankingResultTableDetail(props) {
  const { rowNumber, dept, className, rankingResultEllipsisProps } = props;

  return (
    <RowWrapper className={`col ${className || ""}`}>
      <ColumnWrapper className="group-267">
        <RowNumber className="number-12">{rowNumber}</RowNumber>
        <DocumentNo className="pr20210119001-1">PR20210119001</DocumentNo>
        <DocumentName className="text-21">ชุดการเรียนการสอน สำหรับ...</DocumentName>
        <Department className="dept-2">{dept}</Department>
        <Amount className="number-13">1</Amount>
        <Username className="username01-1">Username01</Username>
        <Price className="price-1">฿10,000.00</Price>
      </ColumnWrapper>
      <Reason className="text-22">
        ซื้อใหม่
        <br />
        ใช้สำหรับทำวิจัยและบริการวิชาการ
        <br />
        ใช้ประกอบการเรียนการสอน
      </Reason>
      <Ellipsis ellipsisProps={rankingResultEllipsisProps.ellipsisProps} />
    </RowWrapper>
  );
}

const RowWrapper = styled.div`
  height: 64px;
  position: relative;
  display: flex;
  padding: 11px 36.7px;
  justify-content: flex-end;
  align-items: flex-start;
  min-width: 1060px;
  background-color: var(--palettewhite);
  border-radius: 10px;

  &.col.col-1 {
    margin-top: 8px;
  }

  &.col.col-2 {
    margin-top: 8px;
  }

  &.col.col-3 {
    margin-top: 8px;
  }

  &.col.col-4 {
    margin-top: 8px;
  }

  &.col.col-5 {
    margin-top: 8px;
  }

  &.col.col-6 {
    margin-top: 8px;
  }
`;

const ColumnWrapper = styled.div`
  ${KanitR14}
  height: 28px;
  align-self: flex-end;
  margin-bottom: 3.12px;
  display: flex;
  align-items: flex-end;
  min-width: 680px;
`;

const RowNumber = styled.div`
  width: 8px;
  min-height: 27px;
  font-weight: 400;
  color: var(--palettetext);
`;

const DocumentNo = styled.div`
  width: 94px;
  min-height: 27px;
  margin-left: 27px;
  font-weight: 400;
  color: var(--blueberry);
`;

const DocumentName = styled.div`
  width: 164px;
  min-height: 27px;
  align-self: flex-start;
  margin-left: 37px;
  font-weight: 400;
  color: var(--palettetext);
`;

const Department = styled.div`
  width: 30px;
  min-height: 27px;
  margin-left: 37px;
  font-weight: 400;
  color: var(--palettetext);
  text-align: center;
`;

const Amount = styled.div`
  width: 8px;
  min-height: 27px;
  margin-left: 37px;
  font-weight: 400;
  color: var(--palettetext);
`;

const Username = styled.div`
  width: 77px;
  min-height: 27px;
  margin-left: 37px;
  font-weight: 400;
  color: var(--palettetext);
  text-align: center;
`;

const Price = styled.div`
  width: 73px;
  min-height: 27px;
  margin-left: 37px;
  font-weight: 400;
  color: var(--palettetext);
`;

const Reason = styled.div`
  ${KanitR12}
  width: 183px;
  min-height: 41px;
  margin-left: 23px;
  font-weight: 400;
  color: var(--palettetext);
  line-height: 14px;
`;

export default RankingResultTableDetail;
