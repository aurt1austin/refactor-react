import React from 'react';
import styled from 'styled-components';
import { KanitR14 } from '../../../../../styledMixins';


function RankingResultStatus() {
  return (
    <Status>
      <StatusText>รอดำเนินการ</StatusText>
    </Status>
  );
}

const Status = styled.div`
  height: 26px;
  margin-left: 717px;
  display: flex;
  padding: 0 14px;
  align-items: center;
  min-width: 102px;
  background-color: var(--egg-sour);
  border-radius: 100px;
`;

const StatusText = styled.div`
  ${KanitR14}
  min-height: 21px;
  min-width: 74px;
  font-weight: 400;
  color: var(--geebung);
`;

export default RankingResultStatus;
