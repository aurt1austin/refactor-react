import React from 'react';
import styled from 'styled-components';
import { Border1pxPaletteblue1, KanitR16 } from '../../../../../styledMixins';


function RejectButton(props) {
  const { children } = props;

  return (
    <Button>
      <Button1>{children}</Button1>
    </Button>
  );
}

const Button = styled.div`
  ${Border1pxPaletteblue1}
  height: 36px;
  display: flex;
  padding: 0 33px;
  align-items: center;
  min-width: 120px;
  border-radius: 4px;
`;

const Button1 = styled.div`
  ${KanitR16}
  min-height: 24px;
  min-width: 54px;
  font-weight: 400;
  color: var(--paletteblue1);
`;

export default RejectButton;
