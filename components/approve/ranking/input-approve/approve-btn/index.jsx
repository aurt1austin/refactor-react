import React from 'react';
import styled from 'styled-components';
import { KanitR16 } from '../../../../../styledMixins';


function ApproveButton(props) {
  const { children } = props;

  return (
    <Button>
      <Button1>{children}</Button1>
    </Button>
  );
}

const Button = styled.div`
  height: 36px;
  margin-left: 16px;
  display: flex;
  padding: 0 40.5px;
  align-items: center;
  min-width: 120px;
  background-color: var(--paletteblue1);
  border-radius: 4px;
`;

const Button1 = styled.div`
  ${KanitR16}
  min-height: 24px;
  min-width: 39px;
  font-weight: 400;
  color: var(--palettewhite);
`;

export default ApproveButton;
