import React from 'react';
import styled from 'styled-components';
import { SarabunR16, SarabunR14 } from '../../../../styledMixins';


function InputApproveRemark(props) {
  const { text_Label, placeholder } = props;

  return (
    <Textarea>
      <TextLabel>{text_Label}</TextLabel>
      <TextAreaWrapper>
        <Placeholder>{placeholder}</Placeholder>
      </TextAreaWrapper>
    </Textarea>
  );
}

const Textarea = styled.div`
  position: absolute;
  width: 699px;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 180px;
`;

const TextLabel = styled.div`
  ${SarabunR16}
  min-height: 21px;
  font-weight: 400;
  color: var(--palettetext);
`;

const TextAreaWrapper = styled.div`
  height: 155px;
  margin-top: 4px;
  display: flex;
  padding: 8px 16px;
  align-items: flex-start;
  min-width: 699px;
  background-color: var(--palettewhite);
  border-radius: 4px;
  border: 1px solid var(--grayscalegray2);
`;

const Placeholder = styled.div`
  ${SarabunR14}
  width: 667px;
  min-height: 18px;
  font-weight: 400;
  color: var(--grayscalegray2);
`;

export default InputApproveRemark;
