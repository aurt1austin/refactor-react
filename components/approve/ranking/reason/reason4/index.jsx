import React from 'react';
import styled from 'styled-components';
import { ArialNarrowBoldAbbey12px, ArialNarrowBoldMountainMist12px, ValignTextMiddle } from '../../../../../styledMixins';


function Reason4(props) {
  const { place } = props;

  return (
    <DivWrapper>
      <ReasonText>
        <Place>{place}</Place>
        <Percent>0,26%</Percent>
      </ReasonText>
      <ProgressDetail>
        <Progress></Progress>
      </ProgressDetail>
    </DivWrapper>
  );
}

const DivWrapper = styled.div`
  width: 140px;
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 32px;
`;

const ReasonText = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: 140px;
`;

const Place = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldAbbey12px}
            width: 76px;
  height: 14px;
  letter-spacing: 0;
`;

const Percent = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldMountainMist12px}
            width: 40px;
  height: 14px;
  margin-left: 24px;
  text-align: right;
  letter-spacing: 0;
`;

const ProgressDetail = styled.div`
  margin-top: 8px;
  display: flex;
  align-items: flex-start;
  min-width: 140px;
  background-color: var(--catskill-white);
  border-radius: 9px;
`;

const Progress = styled.div`
  width: 13px;
  height: 10px;
  background-color: var(--moody-blue);
  border-radius: 6px;
`;

export default Reason4;
