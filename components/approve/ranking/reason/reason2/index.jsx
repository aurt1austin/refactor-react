import React from 'react';
import ProgressDetail from '../progress-detail';
import styled from 'styled-components';
import {
  ArialNarrowBoldAbbey12px,
  ArialNarrowBoldMountainMist12px,
  ValignTextMiddle,
} from '../../../../../styledMixins';

function Reason2(props) {
  const { place } = props;

  return (
    <DivWrapper>
      <ReasonText>
        <Place>{place}</Place>
        <Percent>0,20%</Percent>
      </ReasonText>
      <ProgressDetail />
    </DivWrapper>
  );
}

const DivWrapper = styled.div`
  width: 144px;
  position: relative;
  margin-top: 21px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 33px;
`;

const ReasonText = styled.div`
  width: 115px;
  height: 14px;
  position: relative;
`;

const Place = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldAbbey12px}
            position: absolute;
  width: 76px;
  height: 14px;
  top: 0;
  left: 0;
  letter-spacing: 0;
`;

const Percent = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldMountainMist12px}
            position: absolute;
  width: 40px;
  height: 14px;
  top: 0;
  left: 75px;
  text-align: right;
  letter-spacing: 0;
`;

export default Reason2;
