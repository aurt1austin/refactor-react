import React from 'react';
import styled from 'styled-components';
import { ArialNarrowBoldAbbey12px, ArialNarrowBoldMountainMist12px, ValignTextMiddle } from '../../../../../styledMixins';


function Reason1(props) {
  const { place } = props;

  return (
    <DivWrapper>
      <ReasonText>
        <Place>{place}</Place>
        <Percent>94%</Percent>
      </ReasonText>
      <ReasonProgress>
        <ProgressDetail></ProgressDetail>
      </ReasonProgress>
    </DivWrapper>
  );
}

const DivWrapper = styled.div`
  width: 144px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 33px;
`;

const ReasonText = styled.div`
  width: 115px;
  height: 14px;
  position: relative;
`;

const Place = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldAbbey12px}
            position: absolute;
  width: 76px;
  height: 14px;
  top: 0;
  left: 0;
  letter-spacing: 0;
`;

const Percent = styled.div`
  ${ValignTextMiddle}
  ${ArialNarrowBoldMountainMist12px}
            position: absolute;
  width: 40px;
  height: 14px;
  top: 0;
  left: 75px;
  text-align: right;
  letter-spacing: 0;
`;

const ReasonProgress = styled.div`
  margin-top: 9px;
  display: flex;
  align-items: flex-start;
  min-width: 140px;
  background-color: var(--catskill-white);
  border-radius: 9px;
`;

const ProgressDetail = styled.div`
  width: 128px;
  height: 10px;
  background-color: var(--moody-blue);
  border-radius: 6px;
`;

export default Reason1;
