import React from 'react';
import styled from 'styled-components';


function ProgressDetail(props) {
  const { className } = props;

  return (
    <ProgressWrapper className={`group-61-1 ${className || ""}`}>
      <Progress className="rectangle-304-1"></Progress>
    </ProgressWrapper>
  );
}

const ProgressWrapper = styled.div`
  margin-top: 9px;
  display: flex;
  align-items: flex-start;
  min-width: 140px;
  background-color: var(--catskill-white);
  border-radius: 9px;

  &.group-61-1.group-61-2 {
    margin-top: 8px;
  }
`;

const Progress = styled.div`
  width: 5px;
  height: 10px;
  background-color: var(--moody-blue);
  border-radius: 6px;
`;

export default ProgressDetail;
