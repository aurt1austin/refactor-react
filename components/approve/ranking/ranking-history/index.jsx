import React from 'react';
import styled from 'styled-components';
import { ValignTextMiddle, KanitR16 } from '../../../../styledMixins';


function RankingHistory() {
  return (
    <DivWrapper>
      <RankingHistoryDetail>CMM 50,000,00 บาท</RankingHistoryDetail>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
      <RankingHistoryDetail2>CMM 50,000,00 บาท</RankingHistoryDetail2>
    </DivWrapper>
  );
}

const DivWrapper = styled.div`
  ${KanitR16}
  width: 168px;
  margin-top: 5px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 220px;
`;

const RankingHistoryDetail = styled.div`
  ${ValignTextMiddle}
  height: 24px;
  font-weight: 400;
  color: var(--black);
`;

const RankingHistoryDetail2 = styled.div`
  ${ValignTextMiddle}
  height: 24px;
  margin-top: 4px;
  font-weight: 400;
  color: var(--black);
`;

export default RankingHistory;
