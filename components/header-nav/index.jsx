import React from 'react';
import Logo from './LogoFIET';
import UserHeaderInfo from './UserHeaderInfo';
import styled from 'styled-components';


function HeaderNav() {
  return (
    <HeaderWrapper>
      <Logo />
      <UserHeaderInfo />
    </HeaderWrapper>
  );
}

const HeaderWrapper = styled.div`
  position: fixed;
  height: 61px;
  top: 0;
  left: 315px;
  z-index: 6;
  display: flex;
  padding: 13px 31px;
  align-items: flex-end;
  min-width: 1125px;
  background-color: var(--palettewhite);
`;

export default HeaderNav;
