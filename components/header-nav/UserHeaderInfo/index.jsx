import React from 'react';
import User from './UserIcon';
import styled from 'styled-components';
import { KanitR14, ValignTextMiddle } from '../../../styledMixins';

function UserHeaderInfo() {
  return (
    <UserHeaderInfoWrapper>
      <DivWrapper>
        <User />
      </DivWrapper>
      <Username>Username</Username>
    </UserHeaderInfoWrapper>
  );
}

const UserHeaderInfoWrapper = styled.div`
  margin-left: 685px;
  margin-bottom: 1.98px;
  display: flex;
  align-items: center;
  min-width: 96px;
`;

const DivWrapper = styled.div`
  height: 24px;
  position: relative;
  display: flex;
  padding: 0 4px;
  align-items: center;
  min-width: 24px;
  background-color: var(--grayscalegray3);
  border-radius: 12px;
`;

const Username = styled.div`
  ${ValignTextMiddle}
  ${KanitR14}
            height: 21px;
  margin-left: 8px;
  min-width: 64px;
  font-weight: 400;
  color: var(--palettetext);
`;

export default UserHeaderInfo;
