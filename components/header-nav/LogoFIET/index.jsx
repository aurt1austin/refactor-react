import React from 'react';
import styled from 'styled-components';
import { NunitoBoldPickledBluewood18px } from '../../../styledMixins';


function LogoFIET() {
  return (
    <LogoFlexWrapper>
      <LogoIconGroup>
        <IconRectangleVerticle></IconRectangleVerticle>
        <IconRectangleHorizontal></IconRectangleHorizontal>
        <IconOrange src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/intersect@2x.svg" />
        <IconBlue src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/intersect-1@2x.svg" />
      </LogoIconGroup>
      <LogoText>FIET Asset Management</LogoText>
    </LogoFlexWrapper>
  );
}

const LogoFlexWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: 252px;
`;

const LogoIconGroup = styled.div`
  width: 27px;
  height: 31px;
  position: relative;
  margin-top: -0.46px;
`;

const IconRectangleVerticle = styled.div`
  position: absolute;
  width: 10px;
  height: 31px;
  top: 0;
  left: 0;
  background-color: var(--paletteblue2);
  border-radius: 25.19px;
`;

const IconRectangleHorizontal = styled.div`
  position: absolute;
  width: 10px;
  height: 26px;
  top: -8px;
  left: 9px;
  background-color: var(--paletteblue2);
  border-radius: 25.19px;
  transform: rotate(90deg);
`;

const IconOrange = styled.img`
  position: absolute;
  width: 10px;
  height: 10px;
  top: 12px;
  left: 12px;
`;

const IconBlue = styled.img`
  position: absolute;
  width: 10px;
  height: 10px;
  top: 0;
  left: 0;
`;

const LogoText = styled.div`
  ${NunitoBoldPickledBluewood18px}
  width: 217px;
  min-height: 20px;
  align-self: flex-end;
  margin-left: 9px;
  letter-spacing: 0;
`;

export default LogoFIET;
