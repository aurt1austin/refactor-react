import React from 'react';
import styled from 'styled-components';


function IconChevronRight() {
  return <Icon></Icon>;
}

const Icon = styled.div`
  width: 20px;
  height: 20px;
  margin-left: 4px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/u-angle-right@2x.svg);
  background-size: 100% 100%;
`;

export default IconChevronRight;
