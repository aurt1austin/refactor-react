import React from 'react';
import IconChevronRight from './icon-chevron-right';
import styled from 'styled-components';
import { SarabunB12, SarabunR12 } from '../../styledMixins';


function Breadcrumbs(props) {
  const { mainMenu, subMenu } = props;

  return (
    <BreadcrumbsWrapper>
      <MainMenu>{mainMenu}</MainMenu>
      <IconChevronRight />
      <SubMenu>{subMenu}</SubMenu>
    </BreadcrumbsWrapper>
  );
}

const BreadcrumbsWrapper = styled.div`
  position: absolute;
  height: 20px;
  top: 2px;
  left: 53px;
  display: flex;
  align-items: center;
  min-width: 217px;
`;

const MainMenu = styled.div`
  ${SarabunR12}
  min-height: 16px;
  min-width: 92px;
  font-weight: 400;
  color: var(--grayscalegray1);
`;

const SubMenu = styled.div`
  ${SarabunB12}
  min-height: 16px;
  margin-left: 4px;
  min-width: 97px;
  font-weight: 700;
  color: var(--systeminfo);
`;

export default Breadcrumbs;
