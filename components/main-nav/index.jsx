import React from 'react';
import MainNavIcon from './main-nav-icon';
import MainNavExpandBG from './main-nav-expand/main-nav-expand-bg';
import SubMenuDetail from './main-nav-expand/sub-menu-detail';
import styled from 'styled-components';
import { KanitR30 } from '../../styledMixins';

function MainNav(props) {
  const {
    mainMenuLabel,
    mainNavIconProps,
    menu1Props,
    menu2Props,
    menu3Props,
  } = props;

  return (
    <MainNavWrapper>
      <MainNavIcon {...mainNavIconProps} />
      <MainNavExpand>
        <MainNavExpandBG />
        <MainMenuLabel>{mainMenuLabel}</MainMenuLabel>
        <SubMenuDetailWrapper>
          <SubMenuDetail>{menu1Props.children}</SubMenuDetail>
          <SubMenuDetail className={menu2Props.className}>
            {menu2Props.children}
          </SubMenuDetail>
          <SubMenuDetail className={menu3Props.className}>
            {menu3Props.children}
          </SubMenuDetail>
        </SubMenuDetailWrapper>
      </MainNavExpand>
    </MainNavWrapper>
  );
}

const MainNavWrapper = styled.div`
  position: fixed;
  height: 1884px;
  top: 0;
  left: 0;
  z-index: 7;
  display: flex;
  align-items: flex-start;
  min-width: 330px;
`;

const MainNavExpand = styled.div`
  width: 245px;
  height: 1884px;
  position: relative;
`;

const MainMenuLabel = styled.h1`
  ${KanitR30}
  position: absolute;
  top: 40px;
  left: 24px;
  font-weight: 400;
  color: var(--palettetext);
`;

const SubMenuDetailWrapper = styled.div`
  position: absolute;
  width: 245px;
  top: 119px;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  min-height: 120px;
`;

export default MainNav;
