import React from "react";
import styled from "styled-components";


function MainNavExpandBG() {
  return (
    <MainNavExpandBG1>
      <FixHeight src="" />
    </MainNavExpandBG1>
  );
}

const MainNavExpandBG1 = styled.div`
  position: absolute;
  height: 1884px;
  top: 0;
  left: 0;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;
  min-width: 245px;
  background-color: var(--palettewhite);
`;

const FixHeight = styled.img`
  width: 1px;
  height: 1884px;
`;

export default MainNavExpandBG;
