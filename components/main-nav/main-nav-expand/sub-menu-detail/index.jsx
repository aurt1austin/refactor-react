import React from "react";
import styled from "styled-components";
import { KanitR16 } from "../../../../styledMixins";


function SubMenuDetail(props) {
  const { children, className } = props;

  return (
    <SubMenu className={`sub-menu ${className || ""}`}>
      <SubMenuText>{children}</SubMenuText>
    </SubMenu>
  );
}

const SubMenu = styled.div`
  height: 40px;
  display: flex;
  padding: 0 24px;
  align-items: center;
  min-width: 245px;
  background-color: var(--palettewhite);

  &.sub-menu.sub-menu-2 {
    background-color: var(--grayscalegray4);
  }
`;

const SubMenuText = styled.div`
  ${KanitR16}
  min-height: 24px;
  min-width: 167px;
  font-weight: 400;
  color: var(--palettetext);
`;

export default SubMenuDetail;
