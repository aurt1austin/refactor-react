import React from 'react';
import HomeMenu from './HomeMenu';
import NavMenu from './NavMenu';
import styled from 'styled-components';


function MainNavIcon(props) {
  const {
    navMenuRequestProps,
    navMenuAppoveProps,
    navMenuStatisticsProps,
    navMenuAllocatateProps,
    navMenuManageProps,
  } = props;

  return (
    <MainNavIconWrapper>
      <HomeMenu />
      <NavMenu userProps={navMenuRequestProps.userProps} />
      <NavMenu className={navMenuAppoveProps.className} userProps={navMenuAppoveProps.userProps} />
      <NavMenu userProps={navMenuStatisticsProps.userProps} />
      <HomeMenu className={navMenuAllocatateProps.className} />
      <NavMenu userProps={navMenuManageProps.userProps} />
    </MainNavIconWrapper>
  );
}

const MainNavIconWrapper = styled.div`
  width: 70px;
  position: relative;
  display: flex;
  flex-direction: column;
  padding: 40px 0;
  align-items: center;
  min-height: 1884px;
  background-color: var(--paletteblue1);
`;

export default MainNavIcon;
