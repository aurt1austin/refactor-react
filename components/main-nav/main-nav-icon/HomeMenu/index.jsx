import React from 'react';
import styled from 'styled-components';


function HomeMenu(props) {
  const { className } = props;

  return (
    <Menu className={`menu ${className || ""}`}>
      <Icon
        className="icon-1"
        src="https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icon@2x.svg"
      />
    </Menu>
  );
}

const Menu = styled.div`
  height: 42px;
  display: flex;
  padding: 0 9px;
  align-items: center;
  min-width: 42px;
  background-color: var(--paletteblue1);
  border-radius: 8px;

  &.menu.menu-1 {
    margin-top: 36px;
  }
`;

const Icon = styled.img`
  width: 24px;
  height: 24px;
`;

export default HomeMenu;
