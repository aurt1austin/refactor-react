import React from 'react';
import IconImage from '../../../image-icon';
import styled from 'styled-components';


function NavMenu(props) {
  const { className, userProps } = props;

  return (
    <Menu className={`menu-2 ${className || ""}`}>
      <IconImage className={userProps.className} />
    </Menu>
  );
}

const Menu = styled.div`
  height: 42px;
  position: relative;
  margin-top: 36px;
  display: flex;
  padding: 0 9px;
  align-items: center;
  min-width: 42px;
  background-color: var(--paletteblue1);
  border-radius: 8px;

  &.menu-2.menu-3 {
    background-color: var(--palettelightblue);
  }
`;

export default NavMenu;
