import React from 'react';
import styled from 'styled-components';


function IconImage(props) {
  const { className } = props;

  return <Icon className={`icon-2 ${className || ""}`}></Icon>;
}

const Icon = styled.div`
  width: 24px;
  height: 24px;
  background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/u-file-alt@2x.svg);
  background-size: 100% 100%;

  &.icon-2.icon-3 {
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/u-file-check-alt@2x.svg);
  }

  &.icon-2.icon-4 {
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/u-chart-line@2x.svg);
  }

  &.icon-2.icon-5 {
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/u-setting-1@2x.svg);
  }

  &.icon-2.icon-6 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-7 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-8 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-9 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-10 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-11 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }

  &.icon-2.icon-12 {
    width: 25px;
    margin-top: -1px;
    background-image: url(https://anima-uploads.s3.amazonaws.com/projects/62258f104bd05b8219a856be/releases/6225ea2a365a17a228d5145a/img/icons---standard---more@2x.svg);
  }
`;

export default IconImage;
