import { css } from "styled-components";

export const ValignTextMiddle = css`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const KanitR30 = css`
  font-family: var(--font-family-kanit);
  font-size: var(--font-size-xxxxl);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const KanitR24 = css`
  font-family: var(--font-family-kanit);
  font-size: var(--font-size-xxxl2);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const KanitR16 = css`
  font-family: var(--font-family-kanit);
  font-size: var(--font-size-l);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const KanitR12 = css`
  font-family: var(--font-family-kanit);
  font-size: var(--font-size-s2);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const KanitR14 = css`
  font-family: var(--font-family-kanit);
  font-size: var(--font-size-m);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const SarabunR16 = css`
  font-family: var(--font-family-sarabun);
  font-size: var(--font-size-l);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const SarabunR12 = css`
  font-family: var(--font-family-sarabun);
  font-size: var(--font-size-s2);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const SarabunR14 = css`
  font-family: var(--font-family-sarabun);
  font-size: var(--font-size-m);
  letter-spacing: 0;
  font-weight: 400;
  font-style: normal;
`;

export const SarabunB12 = css`
  font-family: var(--font-family-sarabun);
  font-size: var(--font-size-s2);
  letter-spacing: 0;
  font-weight: 700;
  font-style: normal;
`;

export const RobotoNormalBlack14px = css`
  color: var(--black);
  font-family: var(--font-family-roboto);
  font-size: var(--font-size-m);
  font-weight: 400;
  font-style: normal;
`;

export const RobotoNormalBlueberry16px = css`
  color: var(--blueberry);
  font-family: var(--font-family-roboto);
  font-size: var(--font-size-l);
  font-weight: 400;
  font-style: normal;
`;

export const ArialNarrowBoldAbbey12px = css`
  color: var(--abbey);
  font-family: var(--font-family-arial-narrowbold);
  font-size: var(--font-size-s2);
  font-weight: 700;
  font-style: normal;
`;

export const ArialNarrowBoldMountainMist12px = css`
  color: var(--mountain-mist);
  font-family: var(--font-family-arial-narrowbold);
  font-size: var(--font-size-s2);
  font-weight: 700;
  font-style: normal;
`;

export const PoppinsSemiBoldDeepCove173px = css`
  color: var(--deep-cove);
  font-family: var(--font-family-poppins);
  font-size: var(--font-size-xl);
  font-weight: 600;
  font-style: normal;
`;

export const PoppinsMediumFrenchGray128px = css`
  color: var(--french-gray);
  font-family: var(--font-family-poppins);
  font-size: var(--font-size-s);
  font-weight: 500;
  font-style: normal;
`;

export const PoppinsMediumDeepCove128px = css`
  color: var(--deep-cove);
  font-family: var(--font-family-poppins);
  font-size: var(--font-size-s);
  font-weight: 500;
  font-style: normal;
`;

export const AbelNormalAbbey20px = css`
  color: var(--abbey);
  font-family: var(--font-family-abel);
  font-size: var(--font-size-xxl2);
  font-weight: 400;
  font-style: normal;
`;

export const NunitoBoldPickledBluewood18px = css`
  color: var(--palettetext);
  font-family: var(--font-family-nunito);
  font-size: var(--font-size-xl2);
  font-weight: 700;
  font-style: normal;
`;

export const Border1pxGrayscalegray3 = css`
  border: 1px solid var(--grayscalegray3);
`;

export const Border1pxCararra = css`
  border: 1px solid var(--cararra);
`;

export const Border1pxPaletteblue1 = css`
  border: 1px solid var(--paletteblue1);
`;
