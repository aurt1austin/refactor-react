
import React from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import Ranking from './components/approve/ranking';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/'>
          <Ranking {...rankingProps} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
const navMenuRequestClass = {
    className: '',
};

const navMenuRequestData = {
    userProps: navMenuRequestClass,
};

const navMenuAppoveClass = {
    className: 'icon-3',
};

const navMenuAppoveData = {
    className: 'menu-3',
    userProps: navMenuAppoveClass,
};

const navMenuStatisticsClass = {
    className: 'icon-4',
};

const navMenuStatisticsData = {
    userProps: navMenuStatisticsClass,
};

const navMenuAllocatateData = {
    className: 'menu-1',
};

const navMenuManageClass = {
    className: 'icon-5',
};

const navMenuManageData = {
    userProps: navMenuManageClass,
};

const mainNavIconData = {
    navMenuRequestProps: navMenuRequestData,
    navMenuAppoveProps: navMenuAppoveData,
    navMenuStatisticsProps: navMenuStatisticsData,
    navMenuAllocatateProps: navMenuAllocatateData,
    navMenuManageProps: navMenuManageData,
};

const menu1Data = {
    children: 'อนุมัติเอกสารของภาควิชา',
};

const menu2Data = {
    children: 'อนุมัติเอกสารของคณะฯ',
    className: 'sub-menu-1',
};

const menu3Data = {
    children: 'อนุมัติการจัดอันดับ',
    className: 'sub-menu-2',
};

const mainNavData = {
    mainMenuLabel: 'อนุมัติ',
    mainNavIconProps: mainNavIconData,
    menu1Props: menu1Data,
    menu2Props: menu2Data,
    menu3Props: menu3Data,
};

const breadcrumbs = {
    mainMenu: 'อนุมัติการจัดอันดับ',
    subMenu: 'ปีงบประมาณ 2566',
};

const amoutTabelDetailData1 = {
    className: 'group-227',
};

const amoutTabelDetailData2 = {
    className: 'group-228',
};

const amoutTabelDetailData3 = {
    className: 'group-229',
};

const amoutTabelDetailData4 = {
    className: 'group-230',
};

const amoutTabelDetailData5 = {
    className: 'group-231',
};

const group68Data = {
    place: 'Russia',
};

const group63Data = {
    place: 'Ukraine',
};

const group612Data = {
    className: 'group-61-2',
};

const group210Data = {
    place: 'China',
};

const property1doc5Data = {
    className: 'icon-6',
};

const property1Default41Data = {
    ellipsisProps: property1doc5Data,
};

const rankingResultData1 = {
    rowNumber: '1',
    dept: 'CMM',
    rankingResultEllipsisProps: property1Default41Data,
};

const property1doc6Data = {
    className: 'icon-7',
};

const property1Default42Data = {
    ellipsisProps: property1doc6Data,
};

const rankingResultData2 = {
    rowNumber: '2',
    dept: 'ECT',
    className: 'col-1',
    rankingResultEllipsisProps: property1Default42Data,
};

const property1doc7Data = {
    className: 'icon-8',
};

const property1Default43Data = {
    ellipsisProps: property1doc7Data,
};

const rankingResultData3 = {
    rowNumber: '3',
    dept: 'PTE',
    className: 'col-2',
    rankingResultEllipsisProps: property1Default43Data,
};

const property1doc8Data = {
    className: 'icon-9',
};

const property1Default44Data = {
    ellipsisProps: property1doc8Data,
};

const rankingResultData4 = {
    rowNumber: '4',
    dept: 'MTE',
    className: 'col-3',
    rankingResultEllipsisProps: property1Default44Data,
};

const property1doc9Data = {
    className: 'icon-10',
};

const property1Default45Data = {
    ellipsisProps: property1doc9Data,
};

const rankingResultData5 = {
    rowNumber: '5',
    dept: 'CTE',
    className: 'col-4',
    rankingResultEllipsisProps: property1Default45Data,
};

const property1doc10Data = {
    className: 'icon-11',
};

const property1Default46Data = {
    ellipsisProps: property1doc10Data,
};

const rankingResultData6 = {
    rowNumber: '6',
    dept: 'ETE',
    className: 'col-5',
    rankingResultEllipsisProps: property1Default46Data,
};

const property1doc11Data = {
    className: 'icon-12',
};

const property1Default47Data = {
    ellipsisProps: property1doc11Data,
};

const rankingResultData7 = {
    rowNumber: '7',
    dept: 'PRT',
    className: 'col-6',
    rankingResultEllipsisProps: property1Default47Data,
};

const inputApproveRemarkData = {
    text_Label: 'ความคิดเห็น',
    placeholder: '',
};

const rejectButtonData = {
    children: 'ไม่อนุมัติ',
};

const approveButtonData = {
    children: 'อนุมัติ',
};

const rankingProps = {
    graphHeaderTitle: '5 ปีย้อนหลัง',
    graphHeaderSubTitle: 'Overall Earning',
    sortBy1: 'Sort by:',
    monthly1: 'Monthly',
    scaleY1: '2500',
    scaleY2: '2000',
    scaleY3: '1500',
    scaleY4: '1000',
    scaleY5: '500',
    scaleY6: '0',
    scaleX1: '2561',
    scaleX2: '2562',
    scaleX3: '2563',
    scaleX4: '2564',
    scaleX5: '2565',
    amountEachYearTitle: 'ยอดเงินแต่ละปี ไม่เกิน 5 แสน',
    sortBy2: 'Sort by:',
    monthly2: 'Monthly',
    amountEachYearColumn1: 'ลำดับ',
    amountEachYearColumn2: 'เลขที่เอกสาร',
    amountEachYearColumn3: 'ชื่อรายการ',
    amountEachYearColumn4: 'ภาควิชา',
    amountEachYearColumn5: 'ผู้ยื่น',
    amountEachYearColumn6: 'ยอดรวมสุทธิ',
    rankingHistoryTitle: '5 ปีย้อนหลัง ลำดับ',
    reasonTitle: 'เหตุผล',
    place: 'Mongolia',
    percent: '0,13%',
    rankinResultTitle: 'ผลการจัดอันดับโครงการ',
    rankingResultColumn1: 'เลขที่เอกสาร',
    rankingResultColumn2: 'ชื่อรายการ',
    rankingResultColumn3: 'ภาควิชา',
    rankingResultColumn4: 'ลำดับ',
    rankingResultColumn5: 'ผู้ยื่น',
    rankingResultColumn6: 'ยอดรวมสุทธิ',
    rankingResultColumn7: 'เหตุผล',
    approveTitle: 'อนุมัติเอกสาร',
    approveRemark: '(ไม่บังคับ)',
    mainNavProps: mainNavData,
    breadcrumbsProps: breadcrumbs,
    amoutTabelDetailProps1: amoutTabelDetailData1,
    amoutTabelDetailProps2: amoutTabelDetailData2,
    amoutTabelDetailProps3: amoutTabelDetailData3,
    amoutTabelDetailProps4: amoutTabelDetailData4,
    amoutTabelDetailProps5: amoutTabelDetailData5,
    reasonProps1: group68Data,
    reasonProps2: group63Data,
    reasonProps3: group612Data,
    reasonProps4: group210Data,
    rankingResultProps1: rankingResultData1,
    rankingResultProps2: rankingResultData2,
    rankingResultProps3: rankingResultData3,
    rankingResultProps4: rankingResultData4,
    rankingResultProps5: rankingResultData5,
    rankingResultProps6: rankingResultData6,
    rankingResultProps7: rankingResultData7,
    InputApproveRemarkProps: inputApproveRemarkData,
    rejectButtonProps: rejectButtonData,
    approveButtonProps: approveButtonData,
};

